<?php
	//top content
	$synopsis = esc_html( get_post_meta(get_the_ID(), 'descrption_main', true));

	$mainFeaturedImage = get_post_meta( get_the_ID(), 'featured_image_top', true );
	$mainFeaturedImage = json_decode($mainFeaturedImage);
	$mainFeaturedImage = $mainFeaturedImage->cropped_image;
?>

<div class="production-section">

	<div class="production-section__featured-image">
		<?php echo wp_get_attachment_image( $mainFeaturedImage,'full' ) ?>
	</div>

	<!-- content wrapper -->
	<div class="production-section__content-wrapper">

		<!-- left -->
		<div class="production-section__left">
			
			<?php get_template_part('partials/production/info');//get time, date, title?>

			<?php get_template_part('partials/production/misc');//get misc stuff about the production?>

		</div>

		<!-- right -->
		<div class="production-section__right">

			<div class="production-section__synopsis">
				<p>
					<?php echo $synopsis;?>
				</p>
			</div>


			<?php get_template_part('partials/production/reviews');//reviews?>

			<?php get_template_part('partials/production/writer');//about the writer?>


			<?php get_template_part('partials/production/performers');//about the writer?>

		</div>

		
			    
	    <?php $rows = get_post_meta( get_the_ID(), 'slides', true ); ?>
	    <?php if($rows):?>
	    	<div class="production-section__slider">
	    		<h2>
	    			<?php echo esc_attr('Behind the scenes');?>
	    		</h2>
				<div class="owl-carousel owl-theme">
					<?php foreach ( (array) $rows as $index => $row ) : $field_prefix = 'slides_' . $index . '_'; ?>
						<?php switch ( $row ) : case 'single_image' : ?>
								<?php
									$singleImage = get_post_meta( get_the_ID(), $field_prefix . 'image', true );
									$singleImage = json_decode($singleImage);
									$singleImage = $singleImage->cropped_image;
								?>

								<div class="item">
									<div class="single-imge">
										<?php echo wp_get_attachment_image( $singleImage,'full' ) ?> 
									</div>
								</div>

							<?php break; ?>
							<?php case 'double_image' : ?>
								<?php
									$imageLeft = get_post_meta( get_the_ID(), $field_prefix . 'image_left', true );
									$imageLeft = json_decode($imageLeft);
									$imageLeft = $imageLeft->cropped_image;

									$imageRight = get_post_meta( get_the_ID(), $field_prefix . 'image_right', true );
									$imageRight = json_decode($imageRight);
									$imageRight = $imageRight->cropped_image;
								?>

								<div class="item clearfix">
									<div class="img-left">
										<?php echo wp_get_attachment_image( $imageLeft,'full' ) ?> 
									</div>
									<div class="img-left">
										<?php echo wp_get_attachment_image( $imageRight,'full' ) ?> 
									</div>
								</div>

							<?php break; ?>

						<?php endswitch; ?>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif;?>

			

	</div>

</div>