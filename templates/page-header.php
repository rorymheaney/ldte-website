<?php 
	use Roots\Sage\Titles; 
 	$needPurpleHeader = is_archive() || is_page_template('template-support.php') || is_page_template('template-custom.php') || is_page_template('template-vision.php') || is_home();
 	$getIntroDescription = get_post_meta( get_the_ID(),'description_main_intro_custom',true);
 	$blogDescription = get_field('description_blog_news','option');
?>

<div class="main-intro <?php if($needPurpleHeader):?>main-intro--purple<?php endif;?>">

	<div class="main-intro__row">
		<h1 class="main-intro__title">
			<?= Titles\title(); ?>
		</h1>

		<!-- use fake area for now, they might remove this section -->
			<?php if($getIntroDescription):?>
				<p class="main-intro__description">
					<?php echo $getIntroDescription;?>
				</p>
			<?php elseif(is_category('commissioned-work')):?>
				<p class="main-intro__description">
					<?php the_field('custom_description', 'category_8'); ?>
				</p>
			<?php elseif(is_category('special-events')):?>
				<p class="main-intro__description">
					<?php the_field('custom_description', 'category_15'); ?>
				</p>
			<?php else:?>
				<div class="main-intro__description main-intro__description--purple">
					<?php echo $blogDescription;?>
				</div>
			<?php endif;?>
	</div>

</div>
