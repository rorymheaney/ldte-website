<?php
	$checkImageChoice = get_post_meta( get_the_ID(), 'main_image_type', true );
	//cropped images
	//cropped single
	$imageSingle = get_post_meta( get_the_ID(),'image',true);
	$imageSingle = json_decode($imageSingle);
	$imageSingle = $imageSingle->cropped_image;
	//cropped left
	$imageLeft = get_post_meta( get_the_ID(), 'image_left', true );
	$imageLeft = json_decode($imageLeft);
	$imageLeft = $imageLeft->cropped_image;
	//cropped right 
	$imageRight = get_post_meta( get_the_ID(), 'image_right', true );
	$imageRight = json_decode($imageRight);
	$imageRight = $imageRight->cropped_image;

	$content = get_the_content(); 

	// event template image
	$eventFeaturedImage = get_post_meta( get_the_ID(), 'featured_image_top', true );
	$eventFeaturedImage = json_decode($eventFeaturedImage);
	$eventFeaturedImage = $eventFeaturedImage->cropped_image;


	$linkOffSiteOrDont = get_post_meta( get_the_ID(), 'third_party_link', true ) ? esc_url(get_post_meta( get_the_ID(), 'third_party_link', true )) : get_permalink();
?>
<article <?php post_class('posts-preview__article clearfix'); ?>>

	<div class="posts-preview__image">
		<?php if($checkImageChoice == 'Singular'):?>
			<div class="posts-preview__image-con posts-preview__image-con--single">
				<a href="<?php echo $linkOffSiteOrDont;?>">
					<?php if($eventFeaturedImage):?>
						<?php echo wp_get_attachment_image( $eventFeaturedImage,'full' ) ?>
					<?php else:?>
						<?php echo wp_get_attachment_image( $imageSingle,'full' ) ?>
					<?php endif;?>
					
				</a>
				
			</div>
		<?php else:?>
			<div class="posts-preview__image-con posts-preview__image-con--double clearfix">
				<a href="<?php echo $linkOffSiteOrDont;?>">
					<?php echo wp_get_attachment_image( $imageLeft,'full' ) ?>
					<?php echo wp_get_attachment_image( $imageRight,'full' ) ?>
				</a>
			</div>
		<?php endif;?>
	</div>

    <div class="posts-preview__content">
    	<h2 class="posts-preview__title">
			<a href="<?php echo $linkOffSiteOrDont;?>">
				<?php the_title();?>
			</a>
		</h2>

		<?php if(!is_category('commissioned-work')):?>
			
			<time class="posts-preview__date" datetime="<?= get_the_time('c'); ?>">
				<?= get_the_date(); ?>
			</time>

		<?php endif;?>
		


		<div class="posts-preview__excerpt">
			<p>
				<?php echo wp_trim_words( $content , '25' ); ?>
			</p>
			<?php if(is_category('commissioned-work')):?>
				
				<a class="button button--dark" href="<?php echo $linkOffSiteOrDont;?>">
					<?php echo esc_html( 'Learn More' ); ?>
				</a>

			<?php else:?>
				
				<a class="posts-preview__read-more" href="<?php echo $linkOffSiteOrDont;?>">
					<?php echo esc_html( 'Read More' ); ?>
				</a>

			<?php endif;?>
			

		</div>
	</div>


</article>