<?php 
    while (have_posts()) : the_post(); 
?>

    <article <?php post_class('singular-post'); // see posts.scss in layouts ?>>

        <div class="singular-post__intro">
            <?php get_template_part('partials/posts/acf-header-image');//get custom crops?>
            <?php get_template_part('partials/posts/post-nav');//get post nav?>
        </div>


        <header class="singular-post__header">
            <h1 class="singular-post__entry-title">
                <?php the_title(); ?>
            </h1>
            <?php get_template_part('templates/entry-meta'); ?>
        </header>

        <div class="singular-post__entry-content">
            <?php the_content(); ?>
        </div>

        <?php //comments_template('/templates/comments.php'); ?>
    </article>

<?php endwhile; ?>
