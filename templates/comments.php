<?php
if (post_password_required()) {
  return;
}
?>

<section id="comments" class="comments">

    <?php if(get_comments_number() < 1):?>
        <div class="comments__count-share clearfix">
        <span class="comments__exact-count"><?php comments_number();?></span>
            <div class="share-buttons share-buttons--single">
                <h2 class="font-styles__links">
                    <?php echo esc_html('Share');?>
                </h2>
                <?php echo do_shortcode("[ssba]"); ?>
            </div>
        </div>
    <?php endif;?>
    <?php if (have_comments()) : ?>
        <div class="comments__count-share clearfix">
            <span class="comments__exact-count"><?php comments_number();?></span>
            <div class="share-buttons share-buttons--single">
                <h2 class="font-styles__links">
                    <?php echo esc_html('Share');?>
                </h2>
                <?php echo do_shortcode("[ssba]"); ?>
            </div>
        </div>
        <ol id="all-comments" class="comments__all comment-list">
            <?php wp_list_comments( array(
                'callback' => 'better_comments'
            ) ); ?>
        </ol>

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
            <nav>
                <ul class="pager">
                    <?php if (get_previous_comments_link()) : ?>
                        <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'sage')); ?></li>
                    <?php endif; ?>
                    <?php if (get_next_comments_link()) : ?>
                        <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'sage')); ?></li>
                    <?php endif; ?>
                </ul>
            </nav>
        <?php endif; ?>
    <?php endif; // have_comments() ?>

  <?php if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments')) : ?>
    <div class="alert alert-warning">
        <?php _e('Comments are closed.', 'sage'); ?>
    </div>
  <?php endif; ?>

  <?php 

    // if(get_comments_number() < 1)
    //     {
    //         comment_form([
    //             "title_reply" => "Join the Conversation",
    //             'label_submit' => 'Submit',
    //             'comment_notes_before' => '',
    //             'comment_notes_after' => ''
    //         ]); 
    //     }
    // else
    //     {
    //         comment_form([
    //             "title_reply" => "Join the Conversation",
    //             'label_submit' => 'Submit',
    //             'comment_notes_before' => '',
    //             'comment_notes_after' => ''
    //         ]); 
    //     }

    
  ?>

  
</section>
