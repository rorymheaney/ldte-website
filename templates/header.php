<?php
    $needPurpleHeader = is_archive() || is_page_template('template-custom.php') || is_page_template('template-support.php') || is_page_template('template-vision.php') || is_home();
?>

<header class="header header--main <?php if ( $needPurpleHeader  ):?>header--purple<?php endif;?>">
    
    <div class="header__sticky">
        
        <div class="header__inner-wrap">
            <div class="title-bar header__title-bar" data-responsive-toggle="top-menu" data-hide-for="medium">  
                <a class="show-for-medium" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img src="<?= get_template_directory_uri(); ?>/dist/images/Lower-Depth_Logo_Blue.svg" alt="<?php bloginfo('name'); ?>">  
                </a>

                <button class="menu-icon" type="button" data-toggle>
                    <div class="title-bar-title">Menu</div>
                </button>
                
            </div>
            <div class="header__nav-toggle-md-up clearfix <?php if ( $needPurpleHeader ):?>header__nav-toggle-md-up--purple<?php endif;?>">
                <div class="title-bar-title">Menu</div>
                <button class="menu-icon" type="button" data-toggle></button>
                
            </div>
            <div class="top-bar header__top-bar" id="top-menu">
                <div class="top-bar-left hide-for-small-only">
                    <ul class="menu">
                        <li class="home">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                <?php if ( $needPurpleHeader ):?>
                                    <img src="<?= get_template_directory_uri(); ?>/dist/images/Lower-Depth_Logo_White.svg" alt="<?php bloginfo('name'); ?>">
                                <?php else:?>
                                    <img src="<?= get_template_directory_uri(); ?>/dist/images/Lower-Depth_Logo_Blue.svg" alt="<?php bloginfo('name'); ?>">
                                <?php endif;?>  
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="top-bar-right header__top-bar-right">
                    <div class="header__top-bar-md-close">
                        <span>
                            <img class="header__close-image" src="<?= get_template_directory_uri(); ?>/dist/images/lightbox-close.png" alt="close">  
                        </span>
                    </div>
                    <ul id="site-nav" class="dropdown menu" data-dropdown-menu>
                        <?php if (has_nav_menu('primary_navigation')) :?>
                            <?php wp_nav_menu([
                                'theme_location' => 'primary_navigation', 
                                'menu_class' => 'nav', 
                                'container' => '', 
                                'items_wrap' => '%3$s', 
                                'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()
                            ]);?>
                        <?php endif;?>
                    </ul>

                    <div class="header__contact-us">
                        <div class="header__contact-us-close text-right">
                            <span>
                                <img class="header__close-image" src="<?= get_template_directory_uri(); ?>/dist/images/lightbox-close.png" alt="close">  
                            </span>
                        </div>
                        <h2>
                            <?php echo esc_html('Contact Us');?>
                        </h2>

                        <?php echo gravity_form(2, false, true, false, '', true, 12);?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</header>
