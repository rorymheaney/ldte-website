<?php
/**
 * Template Name: Our Vision
 */
?>


<div class="main-intro main-intro--purple">
	<?php
		$pageDescription = esc_html( get_post_meta(get_the_ID(),'description_opening', true) );
	?>


	<div class="main-intro__row">
		<h1 class="main-intro__title">
			<?php the_title();?>
		</h1>

		<p class="main-intro__description">
			<?php echo $pageDescription;?>
		</p>
	</div>

</div>

<div id="main-actors">
	<?php 

	$posts = get_post_meta( get_the_ID(), 'member_profile_featured', true );

	if( $posts ): ?>
		<ul class="actors-list actors-list--vision actors-list--vision-primary">
        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
            //get your acf meta fields
            
         ?>
            <?php setup_postdata($post); ?>
            	
        	<?php get_template_part('partials/actors/actors');//get actor profile?>

        <?php endforeach; ?>
        </ul>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif; ?>
</div>


<div id="production-actors">
	<?php 

	$posts = get_post_meta( get_the_ID(), 'member_profile_additional', true );

	if( $posts ): ?>
		<ul class="actors-list actors-list--vision actors-list--vision-secondary">
        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
            //get your acf meta fields
            
         ?>
            <?php setup_postdata($post); ?>
            	
        	<?php get_template_part('partials/actors/actors');//get actor profile?>

        <?php endforeach; ?>
        </ul>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif; ?>
</div>