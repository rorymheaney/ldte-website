(function ExtendIncludetemplate(str, data) {
    // match "<% include template-id %>"
    var _underscore_template = _.template;
    _.template = function(str, data) {
        // match "<% include template-id %>"
        return _underscore_template(
            str.replace(
                /<%\s*include\s*(.*?)\s*%>/g,
                function(match, templateId) {
                    var el = jQuery('#' + templateId);
                    return el ? el.html() : '';
                }
            ),
            data
        );
    };
})();