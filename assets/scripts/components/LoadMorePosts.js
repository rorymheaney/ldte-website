var LoadMorePosts = (function ($) {

	//only data variables it needs are from the data attributes -- done
	//create underscores template for easier reading -- done
	//create fail/error handler -- done

	var $loadingSpinner = $('#load-more img');

    //essentially wp_trim_words
    String.prototype.trimToLength = function(m) {
	  return (this.length > m) ? 
	  	jQuery.trim(this).substring(0, m).split(" ").slice(0, -1).join(" ") +"..."+'</p>'
	    : this;
	};

    function init() {



        $loadBtn = $('#load-more');

        $loadBtn.on('click', function () {
            var currentPage = $('#data-parameters').data('page');
            var nextPage = currentPage + 1;
            //console.log(nextPage);
            //if blog page
            var dataSentOver;
            $("#data-parameters").each(function(){
                //console.log(item);
                dataSentOver = $(this).data();
            });

            //console.log(dataSentOver);
            var posts = new wp.api.collections.Posts();
            //console.log(posts);
            var promise = posts.fetch({
                data: dataSentOver
            });
            //console.log(promise);

            $loadingSpinner.fadeIn();
            // Continue when the fetch completes.
            promise
                .done(function (data, textStatus, request) {

                    var underScoreTemplate = _.template($('#recent-fancy-posts').html());
                    var lastPage = parseInt(request.getResponseHeader('X-WP-TotalPages'));

      
                    if (posts.models.length !== 0) {

                        _.each( posts.models, function( post ) {
                            var postInfo = post.attributes;

                            //console.log(postInfo);
                            //console.log(postInfo.title.rendered)
                            var postDate = moment(postInfo.date).format("MMMM D, YYYY"),
                                postContent = postInfo.excerpt.rendered.trimToLength(160),
                                selectType = postInfo.acf.main_image_type;

                            //send values to the main template
                            //these values will also set the values in the partial being included
                            $('#posts-preview-container').append(underScoreTemplate({
                                imageSelect: postInfo.acf.main_image_type,
                                singularImage: (selectType === 'Singular' ? postInfo.acf.image.url: ''),
                                imageLeft: (selectType === 'Double' ? postInfo.acf.image_left.url: ''),
                                imageRight: (selectType === 'Double' ? postInfo.acf.image_right.url: ''),
                                // imageOption: imageConfiguration,
                                theDate: postDate,
                                thePostLink: postInfo.link,
                                thePostTitle: postInfo.title.rendered,
                                thePostConent: postContent
                            }));

                        } );

                        $('#data-parameters').data('page', nextPage);


                        //oh hey, you reached the last page, 
                        //so you've increased the total number of pages there are 
                        //and now the load more button will fade away
                        if(lastPage < nextPage) {
                            $loadBtn.fadeOut();
                        }

                    } else {
                        $loadBtn.fadeOut();
                    }

                    

                    

                })
                .always(function(){
                    //console.log('always');
                    $loadingSpinner.fadeOut();
                })
                .fail(function() {
                    alert( "please refresh and try again" );
              });

            

        });
        
    }

    


    return {
        init: init
    };
    

})(jQuery);