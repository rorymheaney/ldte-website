var SkrollrFun = (function ($) {

	

    function init() {

    	if (Foundation.MediaQuery.atLeast('large')) {
			// True if medium or large
			// False if small
			var s = skrollr.init();
		}

		$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {

			if(newSize === 'small'){
				var s = skrollr.init().destroy();
			} else if(newSize === 'medium'){
				var s = skrollr.init().destroy();
			} else {
				var s = skrollr.init();
			}
		});
    	

    }

    


    return {
        init: init
    };
    

})(jQuery);