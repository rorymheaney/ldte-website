var HomeSliderHeight = (function ($) {

	var $slider = $('#home-slider'),
		$orbitContainer = $('.orbit-container');
	

    function init() {

    	$slider.on('slidechange.zf.orbit',function(idx,chosenSlide){
			//console.log(chosenSlide);
			var getHeight = $(chosenSlide).height();
			//console.log(getHeight);
			$orbitContainer.css('max-height',getHeight);
			//console.log(idx);
		});

		$slider.on('resizeme.zf.trigger', function(handleResize){
			var activeHeight = $('#home-slider .is-active').height();
			$orbitContainer.css('max-height',activeHeight);
		});
        

    }

    


    return {
        init: init
    };
    

})(jQuery);