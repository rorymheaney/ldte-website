var OwlProductionSlider = (function ($) {

	

    function init() {

    	$('.owl-carousel').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    responsive:{
		        0:{
		            items:1
		        }
		    }
		});

    }

    


    return {
        init: init
    };
    

})(jQuery);






