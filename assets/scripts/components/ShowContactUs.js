var ShowContactUs = (function ($) {

	var $contactLink = $('#menu-item-53 a')
		$siteNav = $('#site-nav'),
		$navCloser = $('.header__top-bar-md-close'),
		$contactClose = $('.header__contact-us-close span'),
		$contactUsDiv = $('.header__contact-us');

    function init() {

    	$contactLink.on('click',function(event){
    		event.preventDefault();

    		$navCloser.fadeOut();
    		$siteNav.fadeOut(function(){
    			$contactUsDiv.fadeIn();
    		});
    		
    	});

    	$contactClose.on('click',function(){
    		
    		$contactUsDiv.fadeOut(function(){
    			$navCloser.fadeIn();
    			$siteNav.fadeIn();
    		});
    	});

        $('#menu-item-253 a').on('click',function(event){
            
            event.preventDefault();
            $('.header__nav-toggle-md-up').trigger('click');
            $navCloser.fadeOut();
            $siteNav.fadeOut(function(){
                $contactUsDiv.fadeIn();
            });
        });

    }

    


    return {
        init: init
    };
    

})(jQuery);