var StickyHeaderOnScroll = (function ($) {

	

    function init() {

    	//console.log('teasdfaf a');
        if (Foundation.MediaQuery.atLeast('medium')) {
			$(".header__sticky").sticky({topSpacing:0});
		}

		$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
			if(newSize === 'small'){
				//remove previous class and style and use default menu for mobile
				$(".header__sticky").unstick({topSpacing:0});
			} else {
				$(".header__sticky").sticky({topSpacing:0});
			}
		});

    }

    


    return {
        init: init
    };
    

})(jQuery);