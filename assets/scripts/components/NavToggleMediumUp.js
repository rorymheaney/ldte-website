var NavToggleMediumUp = (function ($) {

	var $menuToggle = $('.header__nav-toggle-md-up'),
		$siteMenu = $('.header__top-bar-right'),
		$siteMenuClose = $('.header__top-bar-md-close span');
	

    function init() {

    	if (Foundation.MediaQuery.atLeast('medium')) {
			// True if medium or large
			// False if small
			togglingMenuandConact();
		}

		$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
			// newSize is the name of the now-current breakpoint, oldSize is the previous breakpoint
			//console.log(newSize);
			//console.log(oldSize);
			if(newSize === 'small'){
				//remove previous class and style and use default menu for mobile
				$siteMenu.removeClass('header__top-bar-right--fixed').removeAttr("style");
			} 
		});

    }

    function togglingMenuandConact(){
    	$menuToggle.on('click',function(){
				
			if(!$siteMenu.hasClass('header__top-bar-right--fixed')){
				$siteMenu.addClass('header__top-bar-right--fixed').fadeIn();
			} else {
				$siteMenu.removeClass('header__top-bar-right--fixed').fadeOut();
			}
		});

		//click X, close menu
		$siteMenuClose.on('click',function(){
			$siteMenu.fadeOut(function(){
				$siteMenu.removeClass('header__top-bar-right--fixed')
			});
		});
    }

    


    return {
        init: init
    };
    

})(jQuery);