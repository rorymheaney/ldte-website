<?php 
	//blog/archive/category paremters for ajax below 
?>

<div id="data-parameters" data-per_page="4" <?php if(is_category()):?>data-categories="<?php echo get_query_var('cat'); ?>"<?php endif;?> <?php if(is_home()):?>data-categories_exclude="<?php echo esc_html('8');?>"<?php endif;?> data-page="2"></div>

<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
	<div class="alert alert-warning">
		<?php _e('Sorry, no results were found.', 'sage'); ?>
	</div>
	<?php get_search_form(); ?>
<?php endif; ?>

<div id="posts-preview-container" class="posts-preview">
	<?php while (have_posts()) : the_post(); ?>
		
		<?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
		
	<?php endwhile; ?>

</div>

<div id="load-more-container" class="clearfix text-center">

	<button id="load-more" class="button button--dark button--load-more">
		<?php echo esc_html('Load More');?> 
		<img src="<?= get_template_directory_uri(); ?>/dist/images/Loading.gif" alt="<?php echo esc_html('loading more');?>">
	</button>

</div>

<?php //the_posts_navigation(); ?>




<script type="text/html" id="recent-fancy-posts">

    <article class="posts-preview__article clearfix"> 
    	<!-- imageOption is built, logic ready to already in get request -->
        <% include post-image-config %>
        <!-- getting post content, basic values -->
        <div class="posts-preview__content"> 
            <h2 class="posts-preview__title">
                <a class="" href="<%= thePostLink %>">
                    <%= thePostTitle %> 
                </a>
            </h2>
            <time class="posts-preview__date" datetime="<%= theDate %> ">
				<%= theDate %> 
			</time>
            <div class="posts-preview__excerpt">
            	<%= thePostConent %>
            	<a class="posts-preview__read-more" href=" <%= thePostLink %> ">
                    Read More
                </a>
            </div>
            
        </div>

    </article>

</script>

<script type="text/html" id="post-image-config">

	<div class="posts-preview__image">
           
        <!-- if slect is single -->
        <% if (imageSelect === "Singular") { %>
		    <div class="posts-preview__image-con posts-preview__image-con--single">
				<a href="<%= thePostLink %>">
					<img src="<%= singularImage %>" alt="image">
				</a>
			</div>
		<!-- else grab half crops -->
		<% } else { %>
	        <div class="posts-preview__image-con posts-preview__image-con--double clearfix">
				<a href="<%= thePostLink %>">
					<img src="<%= imageLeft %>" alt="image">
					<img src="<%= imageRight %>" alt="image">
				</a>
			</div>
	    <% } %> 
    </div>
        
</script>