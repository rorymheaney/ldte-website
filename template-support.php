<?php
/**
 * Template Name: Donate
 */
?>

<?php get_template_part('templates/page', 'header'); ?>

<div class="support-intro">
	<h2 class="support-intro__title">
		<?php echo esc_html('Support Us');?>
	</h2>
</div>

<div class="donate-copy">
	<div class="donate-copy__content">
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('templates/content', 'page'); ?>
		<?php endwhile; ?>
	</div>

	<?php

	// check if the repeater field has rows of data
	if( have_rows('logos') ):?>

	<div class="row small-up-2 medium-up-3 large-up-4">
	    <?php while ( have_rows('logos') ) : the_row(); $sponsorLogo = get_sub_field('logo');?>

		<div class="column column-block">
			<?php if(get_sub_field('logo_link')):?>
				<a href="<?php the_sub_field('logo_link');?>" target="_blank">
					<img src="<?php echo $sponsorLogo['url'];?>">
				</a>
			<?php else:?>
				<img src="<?php echo $sponsorLogo['url'];?>">
			<?php endif;?>
		</div>

	    <?php endwhile;?>
    </div>
	<?php else :?>

	    

	<?php endif;?>

	<div class="donate-copy__form donate-copy__content">
		<?php the_field('form_intro');?>
	</div>
</div>

<iframe id="mc-donation" src="https://app.mobilecause.com/form/lP06Vg" width="100%" height="1321" overflow="scroll" onLoad="window.scrollTo(0,0);"></iframe>