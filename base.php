<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

    <?php get_template_part('templates/head'); ?>

    <body <?php body_class(); ?> data-ajaxurl="<?php echo esc_url(home_url()); ?>">
        <!--[if IE]>
            <div class="alert alert-warning">
            <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
            </div>
        <![endif]-->
        <?php
            do_action('get_header');
            get_template_part('templates/header');
        ?>

        <?php include Wrapper\template_path(); ?>


        <?php
            do_action('get_footer');
            get_template_part('templates/footer');
            wp_footer();
        ?>
    </body>


<!-- 

 _____                               _____                                                                              
/\  __`\                            /\  __`\                                                                            
\ \ \L\_\ __      ___     ___  __  _\ \,\L\_\     __   __  __    __    _ __   __    ____      ___    ___    ___ ___    
 \ \  _\/'__`\  /' _ `\  /'___/\ \/\ \/_\__ \   /'__`\/\ \/\ \ /'__`\ /\`'__/'__`\ /',__\    /'___\ / __`\/' __` __`\  
  \ \ \/\ \L\.\_/\ \/\ \/\ \__\ \ \_\ \/\ \L\ \/\ \L\ \ \ \_\ /\ \L\.\\ \ \/\  __//\__, `\__/\ \__//\ \L\ /\ \/\ \/\ \ 
   \ \_\ \__/.\_\ \_\ \_\ \____\/`____ \ `\____\ \___, \ \____\ \__/.\_\ \_\ \____\/\____/\_\ \____\ \____\ \_\ \_\ \_\
    \/_/\/__/\/_/\/_/\/_/\/____/`/___/> \/_____/\/___/\ \/___/ \/__/\/_/\/_/\/____/\/___/\/_/\/____/\/___/ \/_/\/_/\/_/
                                   /\___/            \ \_\                                                             
                                   \/__/              \/_/       

 -->

</html>
