<?php get_template_part('partials/home/preview-slider'); //production slides, orbit slider ?>

<section id="front-page-content">

	<?php get_template_part('partials/home/about'); //about tab (admin) ?>

	<?php get_template_part('partials/home/play'); //submit a play tab (admin) ?>

	<?php get_template_part('partials/home/donate'); //donate tab (admin) ?>

	<?php get_template_part('partials/home/testimonial'); //donate tab (admin) ?>

</section>