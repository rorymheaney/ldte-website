<?php
/**
 * Template Name: Programming
 */
?>

<section id="programming-page-content">

	<?php get_template_part('partials/programming/on-stage'); //on_stage category ?>

	<?php get_template_part('partials/programming/in-depth'); //in_depth category ?>

	<?php get_template_part('partials/programming/breaking-ground'); //breaking ground category ?>

</section>
