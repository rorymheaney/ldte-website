<?php
	$args = array(            
		'post_type' => 'production',
		'posts_per_page' => -1,
		'productions_category' => 'on-stage',
		'meta_key'		=> 'buy_tickets',  	// Change to reflect the name of your custom field
		'meta_value'	=> 'buy-now'			// Change to reflect the name of the value in your custom field
	);
	//$query = query_posts($args);
	$onStageTickets = new WP_Query( $args );		
?>

	<div class="pfc pfc--purple pfc--bg">

		<div class="pfc__row">

			<h2 class="pfc__featured-title">
				<?php echo esc_html('On Stage');?>
			</h2>
			<?php if($onStageTickets->have_posts()): //anything on stage right now??>
				<?php  
					while ($onStageTickets->have_posts()) : $onStageTickets->the_post();
				?>

				<div class="pfc__content-wrapper pfc__content-wrapper--lighter">
					<?php get_template_part('partials/programming-featured-content'); //pfc for short (css naming) ?>
				</div>



				 <?php 
				 	endwhile; 
					wp_reset_query();
				?>
			<?php endif;?>
		</div>
	</div>




<!-- past shows for on stage -->
<?php
	$args = array(            
		'post_type' => 'production',
		'posts_per_page' => -1,
		'productions_category' => 'on-stage',
		'meta_query' => array(
		    array(
		     'key' => 'buy_tickets', // check acf key
		     'compare' => 'NOT IN', // make sure it isn't in there
		     'value' => 'buy-now' // make sure it's not this value
		    )
		)
	);
	//$query = query_posts($args);
	$onStagePast = new WP_Query( $args );		
?>
<div class="pfc pfc--purple pfc--bg pfc--past-productions pfc--show-link">
	<div class="blah">
	</div>
	<span class="pfc__basic-intro pfc__basic-intro--purple">
		<?php echo esc_html('Past Productions');?>
	</span>
	<div class="pfc__row">

		<?php  
			while ($onStagePast->have_posts()) : $onStagePast->the_post();
		?>


			<div class="pfc__content-wrapper pfc__content-wrapper--lighter">
				<?php get_template_part('partials/programming-featured-content'); //pfc for short (css naming) ?>
			</div>


		 <?php 
		 	endwhile; 
			wp_reset_query();
		?>
	</div>
</div>