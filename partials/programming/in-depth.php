
<?php
	$args = array(            
		'post_type' => 'production',
		'posts_per_page' => -1,
		'productions_category' => 'in-depth',
		'meta_key'		=> 'buy_tickets',  	// Change to reflect the name of your custom field
		'meta_value'	=> 'buy-now'			// Change to reflect the name of the value in your custom field
	);
	//$query = query_posts($args);
	$inDepthTickets = new WP_Query( $args );	

	//get in depth options

?>
<div class="pfc pfc--spacing-big">

	<div class="pfc__row">

		<h2 class="pfc__featured-title">
			<?php echo esc_html('In Depth');?>
		</h2>
		<?php //echo print_r($category);?>
		<div class="pfc__category-intro">
			<h3 class="pfc__sub-title">

				<?php
					the_field( 'description_title', 'productions_category_5' ); 
				?>
			</h3>
			<p class="pfc__description">
				<?php
					the_field( 'description_plus', 'productions_category_5' ); 
				?>
			</p>
		</div>

		<?php if($inDepthTickets->have_posts()): //anything in depth right now? ?>

			<?php  
				while ($inDepthTickets->have_posts()) : $inDepthTickets->the_post();
			?>

			<div class="pfc__content-wrapper">
				<?php get_template_part('partials/programming-featured-content'); //pfc for short (css naming) ?>
			</div>



			 <?php 
			 	endwhile; 
				wp_reset_query();
			?>
			
		<?php endif;?>
	</div>
</div>


<!-- past shows for in depth -->
<?php
	$args = array(            
		'post_type' => 'production',
		'posts_per_page' => -1,
		'productions_category' => 'in-depth',
		'meta_query' => array(
		    array(
		     'key' => 'buy_tickets', // check acf key
		     'compare' => 'NOT IN', // make sure it isn't in there
		     'value' => 'buy-now' // make sure it's not this value
		    )
		)
	);
	//$query = query_posts($args);
	$inDepthpast = new WP_Query( $args );		
?>
<div class="pfc pfc--past-productions pfc--show-link">
	<span class="pfc__basic-intro">
		<?php echo esc_html('Past Readings');?>
	</span>
	<div class="pfc__row">
		<?php  
			while ($inDepthpast->have_posts()) : $inDepthpast->the_post();
		?>


			<div class="pfc__content-wrapper">
				<?php get_template_part('partials/programming-featured-content'); //pfc for short (css naming) ?>
			</div>


		 <?php 
		 	endwhile; 
			wp_reset_query();
		?>
	</div>
</div>