<?php
	$productionReviews = get_post_meta( get_the_ID(), 'reviews_production', true );
	if( $productionReviews ):
?>	
	<div class="production-section__reviews">
		<h2 class="production-section__basic-title">
			<?php echo esc_html('Reviews');?>
		</h2>
		<ul class="production-section__review-list">
			<?php 
				for( $i = 0; $i < $productionReviews; $i++ ):
				$description = esc_html( get_post_meta( get_the_ID(), 'reviews_production_' . $i . '_description', true ) );
				$reviewedBy = esc_html( get_post_meta( get_the_ID(), 'reviews_production_' . $i . '_reviewed_by', true ) );
			?>

				<li class="production-section__review-list-item">
					<?php echo $description;?> <span>- <?php echo $reviewedBy;?></span>
				</li>

			<?php endfor;?>
		</ul>
	</div>
<?php endif;?>