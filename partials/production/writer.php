<?php
	//top content
	$aboutTheWriter = nl2br(esc_html( get_post_meta(get_the_ID(), 'writer_description', true)));
	$aboutTheDirector = nl2br(esc_html( get_post_meta(get_the_ID(), 'direction_description', true)));
	$aboutTheProducer = nl2br(esc_html( get_post_meta(get_the_ID(), 'producer_description', true)));


?>

<?php if($aboutTheWriter):?>
	<div class="production-section__writer">
		<h2 class="production-section__basic-title">
			<?php echo esc_html('About the Writer');?>
		</h2>
		<p>
			<?php echo $aboutTheWriter;?>
		</p>
	</div>
<?php endif;?>

<?php if($aboutTheDirector):?>
	<div class="production-section__writer">
		<h2 class="production-section__basic-title">
			<?php echo esc_html('About the Director');?>
		</h2>
		<p>
			<?php echo $aboutTheDirector;?>
		</p>
	</div>
<?php endif;?>

<?php if($aboutTheProducer):?>
	<div class="production-section__writer">
		<h2 class="production-section__basic-title">
			<?php echo esc_html('About the Producer');?>
		</h2>
		<p>
			<?php echo $aboutTheProducer;?>
		</p>
	</div>
<?php endif;?>