<?php
	// $directedBy = $titleC = esc_html( get_post_meta( get_the_id(), 'directed_by', true ) );
	$dramaturgyBy = $titleC = esc_html( get_post_meta( get_the_id(), 'dramaturgy_by', true ) );
	$dialectWorkBy = $titleC = esc_html( get_post_meta( get_the_id(), 'dialect_work_by', true ) );
	$castingBy = $titleC = esc_html( get_post_meta( get_the_id(), 'casting_by', true ) );
	$stageManagedBy = $titleC = esc_html( get_post_meta( get_the_id(), 'stage_managed_by', true ) );
?>

<ul class="production-section__misc">
	

	<?php

	// check if the repeater field has rows of data
	if( have_rows('extra_event_info') ):?>

	    <?php while ( have_rows('extra_event_info') ) : the_row();?>
	        
	        <li class="production-section__misc-item">
				<span>
					<?php the_sub_field('title');?>
				</span>
				<?php the_sub_field('persons_name');?>
			</li>

	    <?php endwhile;?>

	<?php else :?>

	   

	<?php endif;

	?>
</ul>