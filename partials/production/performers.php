<?php 

$posts = get_post_meta( get_the_ID(), 'production_actors', true );

if( $posts ): ?>
    <div class="production-section__performers">
		<h2 class="production-section__basic-title">
			<?php echo esc_html('Featuring');?>
		</h2>
		<ul class="actors-list actors-list--basic clearfix">
        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
            //get your acf meta fields
            
         ?>
            <?php setup_postdata($post); ?>
            	
        	<?php get_template_part('partials/actors/actors');//get actor profile?>

        <?php endforeach; ?>
        </ul>
	</div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>