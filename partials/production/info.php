<?php
	$productionBy = get_post_meta(get_the_ID(), 'written_by', true);
	$venueName = esc_html( get_post_meta( get_the_ID(), 'venue', true ) );
    $venueAddress = esc_html( get_post_meta(get_the_ID(), 'address', true) );
	$startDate = get_post_meta( get_the_ID(), 'start_date', true );
	$startFormatted = date("F jS",strtotime($startDate));
	$endDate = get_post_meta( get_the_ID(), 'end_date', true );
	$endFormatted = date("F jS",strtotime($endDate));
	$timeOfProduction = get_post_meta( get_the_ID(), 'time_of_production', true );
	$ticketURL = esc_url(get_post_meta( get_the_ID(), 'external_link', true ));
	$synopsis = esc_html( get_post_meta(get_the_ID(), 'descrption_main', true));

	$changeTicketText = get_post_meta(get_the_ID(), 'button_text_tickets_buy', true); 
?>


<h1 class="production-section__title">
	<?php the_title();?>
</h1>

<?php if ($productionBy):?>
<span class="production-section__written-by">
	<?php echo esc_html('Written By');?> - <?php echo $productionBy;?>
</span>
<?php endif;?>

<ul id="extra-misc" class="production-section__misc">
	<li class="production-section__misc-item">
		<span>
			Directed by
		</span>
		<?php the_field('directed_by');?>
	</li>
</ul>

<?php if($ticketURL):?>

	<a href="<?php echo $ticketURL;?>" class="production-section__buy-now button button--dark">
		
		<?php if($changeTicketText === 'buy-now'):?>
			<?php echo esc_html('Buy Tickets');?>
		<?php else:?>
			<?php echo esc_html('Reserve Tickets');?>
		<?php endif;?>
	</a>

<?php endif;?>

<div class="production-section__venue-details">
	 <p class="production-section__date production-section__date--space">
		<?php echo $startFormatted;?> <?php if($endDate):?>- <?php echo $endFormatted;?><?php endif;?>
	</p> 


	
	<?php
		$additionalTimesDates = get_post_meta( get_the_ID(), 'additional_dates_and_times', true );
		if( $additionalTimesDates ):
	?>

		<ul class="production-section__date-list">
			<li class="production-section__date production-section__date--title">
				<?php echo esc_html('Production Time and Dates');?>
			</li>
			<?php 
				for( $x = 0; $x < $additionalTimesDates; $x++ ):
				$field_prefix = 'additional_dates_and_times_' . $x . '_';
				$moreDates = esc_html( get_post_meta( get_the_ID(), $field_prefix . 'date', true ) );
				$moreTimes = esc_html( get_post_meta( get_the_ID(), $field_prefix . 'time', true ) );
			?>

				<li class="production-section__date">
					<?php echo $moreDates?> <?php if($moreTimes):?> - <?php echo $moreTimes?><?php endif;?>
				</li>

			<?php endfor;?>
		
		</ul>

	<?php endif;?>

	<p class="production-section__location">
		<?php echo $venueName; ?> <br/>
    	<?php echo $venueAddress;?>
	</p>


</div>

