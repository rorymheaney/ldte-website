<?php 
	$checkImageChoice = get_post_meta( get_the_ID(), 'main_image_type', true );
    //cropped images
    //cropped single
    $imageSingle = get_post_meta( get_the_ID(),'image',true);
    $imageSingle = json_decode($imageSingle);
    $imageSingle = $imageSingle->cropped_image;
    //cropped left
    $imageLeft = get_post_meta( get_the_ID(), 'image_left', true );
    $imageLeft = json_decode($imageLeft);
    $imageLeft = $imageLeft->cropped_image;
    //cropped right 
    $imageRight = get_post_meta( get_the_ID(), 'image_right', true );
    $imageRight = json_decode($imageRight);
    $imageRight = $imageRight->cropped_image;

    //above variables are used several times, think of a better way to consolidate that that is resuable
?>

<div class="singular-post__image">
    <img class="singular-post__pattern singular-post__pattern--left"  src="<?= get_template_directory_uri(); ?>/dist/images/block2A.png" alt="<?php echo esc_html('pattern image');?>">
    <?php if($checkImageChoice == 'Singular'):?>
        <div class="singular-post__image-con singular-post__image-con--single">
            <?php echo wp_get_attachment_image( $imageSingle,'full' ) ?> 
        </div>
    <?php else:?>
        <div class="singular-post__image-con singular-post__image-con--double clearfix">
            <?php echo wp_get_attachment_image( $imageLeft,'full' ) ?>
            <?php echo wp_get_attachment_image( $imageRight,'full' ) ?>
        </div>
    <?php endif;?>
     <img class="singular-post__pattern singular-post__pattern--right"  src="<?= get_template_directory_uri(); ?>/dist/images/block2A.png" alt="<?php echo esc_html('pattern image');?>">   
</div>