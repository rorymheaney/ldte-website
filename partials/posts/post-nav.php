<div class="singular-post__nav">
	<?php
		$next_post = get_next_post();
		if (!empty( $next_post )): 
	?>
		<div class="singular-post__nav-next" style="background-image: url(<?php echo $nextThumbURL[0];?>);">
		    <a class="singular-post__nav-text-link" href="<?php echo get_permalink( $next_post->ID ); ?>">
		        <img class=""  src="<?= get_template_directory_uri(); ?>/dist/images/leftarrowblack.png" alt="<?php echo esc_html('Previous Post');?>">
		    </a>
		</div>
	<?php endif; ?>



	<?php
		$prev_post = get_previous_post();
		if (!empty( $prev_post )): 
	?>
		<div class="singular-post__nav-previous" style="background-image: url(<?php echo $prevThumbURL[0];?>);">
		    <a class="singular-post__nav-text-link" href="<?php echo get_permalink( $prev_post->ID ); ?>">
		        <img class=""  src="<?= get_template_directory_uri(); ?>/dist/images/rightarrowblack.png" alt="<?php echo esc_html('Next Post');?>">
		    </a>
		</div>
	<?php endif; ?>
</div>