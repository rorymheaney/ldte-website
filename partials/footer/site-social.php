<?php
	//social icons
	$instagramG = esc_url( get_option('options_instagram_global') ); 
	$pinterestG = esc_url( get_option('options_pinterest_global') ); 
	$facebookG = esc_url( get_option('options_facebook_global') ); 
	$youtubeG = esc_url( get_option('options_youtube_global') ); 
	$twitterG = esc_url( get_option('options_twitter_global') ); 
	$rssG = esc_url( get_option('options_rss_global') ); 
?>

<ul class="site-info__social">
	<?php if($instagramG):?>
		<li>
			<a href="<?php echo $instagramG;?>">
				<i class="fa fa-instagram" aria-hidden="true"></i>
			</a>
		</li>
	<?php endif;?>
	<?php if($pinterestG):?>
		<li>
			<a href="<?php echo $pinterestG;?>">
				<i class="fa fa-pinterest" aria-hidden="true"></i>
			</a>
		</li>
	<?php endif;?>
	<?php if($facebookG):?>
		<li>
			<a href="<?php echo $facebookG;?>">
				<i class="fa fa-facebook" aria-hidden="true"></i>
			</a>
		</li>
	<?php endif;?>
	<?php if($youtubeG):?>
		<li>
			<a href="<?php echo $youtubeG;?>">
				<i class="fa fa-youtube-play" aria-hidden="true"></i>
			</a>
		</li>
	<?php endif;?>
	<?php if($twitterG):?>
		<li>
			<a href="<?php echo $twitterG;?>">
				<i class="fa fa-twitter" aria-hidden="true"></i>
			</a>
		</li>
	<?php endif;?>
	<?php if($rssG):?>
		<li>
			<a href="<?php echo $rssG;?>">
				<i class="fa fa-rss" aria-hidden="true"></i>
			</a>
		</li>
	<?php endif;?>
</ul>