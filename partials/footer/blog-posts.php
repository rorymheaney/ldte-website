<div class="footer-posts">
	<h2 class="footer-posts__intro-title">
		<?php echo esc_html('From The Blog');?>
	</h2>

	<div class="footer-posts__post-container" data-equalizer="footer-eq" data-equalize-on-stack="true">
		<?php
			$args = array(            
				'post_type' => 'post',
				'posts_per_page' => 4, 
				'cat' => 9
			);
			//$query = query_posts($args);
			$footerPosts = new WP_Query( $args );		
		?>
		<?php  
			while ($footerPosts->have_posts()) : $footerPosts->the_post();
			//use get_post_meta to limit queries
			//check image choice
			$checkImageChoice = get_post_meta( get_the_ID(), 'main_image_type', true );
			//cropped images
			//cropped single
			$imageSingle = get_post_meta( get_the_ID(),'image',true);
			$imageSingle = json_decode($imageSingle);
			$imageSingle = $imageSingle->cropped_image;
			//cropped left
			$imageLeft = get_post_meta( get_the_ID(), 'image_left', true );
			$imageLeft = json_decode($imageLeft);
			$imageLeft = $imageLeft->cropped_image;
			//cropped right 
			$imageRight = get_post_meta( get_the_ID(), 'image_right', true );
			$imageRight = json_decode($imageRight);
			$imageRight = $imageRight->cropped_image; 
		?>

		<div class="footer-posts__post">

			<div class="footer-posts__content">
				<?php if($checkImageChoice == 'Singular'):?>
					<div class="footer-posts__image footer-posts__image--single">
						<a href="<?php the_permalink();?>">
							<?php echo wp_get_attachment_image( $imageSingle,'medium_large' ) ?>
						</a>
					</div>
				<?php else:?>
					<div class="footer-posts__image footer-posts__image--double">
						<a href="<?php the_permalink();?>">
							<?php echo wp_get_attachment_image( $imageLeft,'medium_large' ) ?>
							<?php echo wp_get_attachment_image( $imageRight,'medium_large' ) ?>
						</a>
					</div>
				<?php endif;?>
				<div class="footer-posts__spacing">
					<ul class="categories-list categories-list--footer">
						<?php 
							$sep = '';
							foreach((get_the_category()) as $cat) {
								//print_r($cat);
								if($cat->term_id !== 9){
									echo $sep . '<li class="categories-list__category"><a class="categories-list__href font__sub-head" href="' . get_category_link($cat->term_id) . '"  title="View all posts in '. esc_attr($cat->name) . '">' . $cat->cat_name . '</a></li>';
								}
								
								// $sep = ', ';
								$sep = '';
							}
							//$category_link = get_category_link( 9 );
						?>
						<!-- <li class="categories-list__category">
							<a class="categories-list__href font__sub-head" href="<?php echo esc_url( $category_link );?>">
								<?php echo esc_html('Featured');?>
							</a>
						</li> -->
					</ul>

					<div class="footer-posts__eq-height" data-equalizer-watch="footer-eq">
						<h2 class="footer-posts__title">
							<a href="<?php the_permalink();?>">
								<?php the_title();?>
							</a>
						</h2>
					</div>
					
					<span class="footer-posts__date">
						<?php the_time('F j, Y'); ?>
					</span>
				</div>
				
			</div>
			
		</div>
		



		 <?php 
		 	endwhile; 
			wp_reset_query();
		?>
	</div>

</div>