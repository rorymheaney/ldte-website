<div class="site-info">
	
	<div class="site-info__row">
		<div class="site-info__block site-info__block--logo">
			<a href="<?php echo esc_url(home_url()); ?>">
				<img src="<?= get_template_directory_uri(); ?>/dist/images/footerlogo.png" alt="<?php bloginfo('name'); ?>">
			</a>
		</div>
		<div class="site-info__block site-info__block--nav">
			<?php wp_nav_menu( array(
			    'menu' => 'Footer'
			) ); ?>
		</div>
		<div class="site-info__block site-info__block--newsletter">
			<h2 class="site-info__form-title">
				<?php echo esc_html('Join our mailing list');?>
			</h2>
			<?php echo gravity_form(1, false, false, false, '', true, 12);?>

			

			<?php get_template_part('partials/footer/site-social'); ?>
		</div>
	</div>

	<div class="site-info__row site-info__row--bottom">
		<div class="site-info__support">
			<h2 class="site-info__support-title">
				<?php echo esc_html('Supported by:');?>
			</h2>

			<?php get_template_part('partials/footer/support-list'); ?>

		</div>
	</div>

</div>
<div class="copy-right-by">
	<div class="copy-right-by__row">
		<div class="copy-right-by__block">
			<span>
				<?php echo esc_html('© 2016 Lower Depth Theatre Ensemble');?>
			</span> 
		</div>
		<div class="copy-right-by__block">
			<a href="<?php echo esc_url('http://projectmplus.com/');?>" target="_blank">
				<?php echo esc_html('Design by Project M Plus');?> 
			</a>
		</div>
	</div>
</div>