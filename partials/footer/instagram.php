<!-- replace once then send over their credentials -->
<?php	
	// if(($instagramObject = get_transient("ldte_instagram")) === false) 
	// {
	// 	set_transient("ldte_instagram", $instagramObject, 3600);
	// 	$userid = 1014885369;
	// 	$clientid = '57cb4205deda4818aecc44b869bd6e7c';
	// 	$num_photos = 20; 
	// 	$instagramAPI = file_get_contents('https://api.instagram.com/v1/users/' . $userid . '/media/recent?access_token=1014885369.1677ed0.76d00c3da9244e2e89794f260e51bcba&client_id:'.$clientid.'&count:'.$num_photos);
	// 	$instagramObject = json_decode( $instagramAPI , true );
	// }
	$userid = 1014885369;
	$clientid = '57cb4205deda4818aecc44b869bd6e7c';
	$num_photos = 20; 
	$instagramAPI = file_get_contents('https://api.instagram.com/v1/users/self/media/recent/?access_token=1014885369.1677ed0.76d00c3da9244e2e89794f260e51bcba');
	$instagramObject = json_decode( $instagramAPI , true );
	
	
// If we have instagramObject
if ( false !== $instagramObject ) : ?>

   	<?php //print_r($instagramObject['data']);?>

   	<ul class="instagram-list clearfix">
	   <?php $countInsta = 1; foreach( $instagramObject['data'] as $image ):?>
	   <?php if ($countInsta%4 == 1)
    {  
         echo '<div class="instagram-list__divs clearfix">';
    }?>
	   	<li class="instagram-list__item">
	   		<a href="<?php echo $image['link'];?>" class="instagram-list__likes" target="_blank">
	   			<span class="wrap">
					<i class="fa fa-heart" aria-hidden="true"></i>
					<span class="count">
						<?php echo $image['likes']['count'];?>
					</span>
				</span>
	   		</a>
	   		<img src="<?php echo $image['images']['standard_resolution']['url'];?>">
	   	</li>
	   	<?php if ($countInsta%4 == 0)
    {  
         echo "</div>";
    }?>
	   <?php  $countInsta++; endforeach;?>
   </ul>
<?php endif;?>
