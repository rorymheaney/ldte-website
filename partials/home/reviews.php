<?php
	$productName = esc_html( get_post_meta( get_the_ID(), 'for_what_production', true ) );
	$reviewText = nl2br(esc_html( get_post_meta( get_the_ID(), 'excerpt', true ) ));
	$ticketLink = esc_url( get_post_meta( get_the_ID(), 'button_link', true ) );

?>

<div class="pfc__content pfc__content--long-slide">
	<span class="pfc__misc">
	        <?php echo esc_html('Reivew');?>
	    </span>

	    <h2 class="pfc__title pfc__title--plus">
        <?php echo $productName;?>
    </h2>

    <div class="copy-long">
	    <?php echo $reviewText;?>
	</div>

    <span class="pfc__misc pfc__misc--bottom">
	        <?php the_title();?>
	    </span>

    <a class="button button--tickets" href="<?php echo $ticketLink;?>" target="_blank">
	        <?php echo esc_html('Read More');?>
	    </a>
</div>