<?php 
	$titleA = esc_html( get_post_meta( get_the_id(), 'title_about', true ) );
	$contentA = get_post_meta( get_the_id(), 'description_about', true);
	$linkTextA = esc_html( get_post_meta( get_the_id(), 'link_text_about', true ) );
	$pageLinkA = get_post_meta( get_the_id(), 'link_page_about', ture );
	$imageA = (int) get_post_meta( get_the_ID(), 'image', true );
?>
<div class="front-block front-block--about">

	<div class="front-block__mobile-img show-for-small-only">
		<?php echo wp_get_attachment_image( $imageA,'medium_large' ) ?>
	</div>

	<div class="front-block__content">
		<h2 class="front-block__title">
			<?php echo $titleA;?>
		</h2>
		<p class="front-block__copy">
			<span>—</span><?php echo $contentA;?>
		</p>

		<a class="front-block__page-link" href="<?php echo get_page_link($pageLinkA); ?>">
			<?php echo $linkTextA;?>
		</a>
	</div>

	<div class="front-block__images">
		<?php echo wp_get_attachment_image( $imageA,'full' ) ?>
		<img data-200-bottom="top: 300px;" data--800-bottom="top: 445px;" class="front-block__img-position front-block__img-position--one" src="<?= get_template_directory_uri(); ?>/dist/images/circleA.png">
		<img data-200-bottom="bottom: -150px;" data--800-bottom="bottom: -65px;" class="front-block__img-position front-block__img-position--two" src="<?= get_template_directory_uri(); ?>/dist/images/blockA.png">
		<img data-200-bottom="bottom: -155px;" data--800-bottom="bottom: -275px;" class="front-block__img-position front-block__img-position--three" src="<?= get_template_directory_uri(); ?>/dist/images/block2A.png">
	</div>
</div>