<?php 
	$titleB = esc_html( get_post_meta( get_the_id(), 'title_play', true ) );
	$contentB = get_post_meta( get_the_id(), 'description_play', true);
	$linkTextB = esc_html( get_post_meta( get_the_id(), 'link_text_play', true ) );
	$pageLinkB = get_post_meta( get_the_id(), 'link_page_play', true );
	$imageB = (int) get_post_meta( get_the_ID(), 'image_play', true );
?>
<div class="front-block front-block--play">
	<div class="front-block__mobile-img show-for-small-only">
		<?php echo wp_get_attachment_image( $imageB,'medium_large' ) ?>
	</div>
	<div class="front-block__images">
		<?php echo wp_get_attachment_image( $imageB,'full' ) ?>
		<img data-100-bottom="bottom: -250px;" data--500-bottom="bottom: -125px;" class="front-block__img-position front-block__img-position--one" src="<?= get_template_directory_uri(); ?>/dist/images/imageB.png">
	</div>

	<div class="front-block__content">
		<h2 class="front-block__title">
			<?php echo $titleB;?>
		</h2>
		<p class="front-block__copy">
			<span>—</span><?php echo $contentB;?>
		</p>

		<a class="front-block__page-link" href="<?php echo get_page_link($pageLinkB);?>">
			<?php echo $linkTextB;?>
		</a>
	</div>

	
</div>