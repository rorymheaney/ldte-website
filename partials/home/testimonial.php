<?php 

//get relationship field
//foundation default orbit slider
$posts = get_post_meta( get_the_ID(), 'testimonial', true );

if( $posts ): ?>

        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
            //get your acf meta fields
            $personsName = esc_html( get_post_meta( get_the_ID(), 'persons_name', true ) );
         ?>
            <?php setup_postdata($post); ?>
            <div class="front-block front-block--testimonial">
                <img data-200-bottom="opacity: 0;" data--700-bottom="opacity: 1;" class="front-block__img-position front-block__img-position--one" src="<?= get_template_directory_uri(); ?>/dist/images/circleA.png">
                <?php the_content();?>
                <div class="front-block__name">
                    <span>
                        <span>—</span> <?php echo $personsName;?>
                    </span>
                </div>
            </div>
        <?php endforeach; ?>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>