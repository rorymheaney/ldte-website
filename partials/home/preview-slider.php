<?php 

//get relationship field
//foundation default orbit slider
$posts = get_post_meta( get_the_ID(), 'featured_productions', true );

if( $posts ): ?>
    <div id="home-slider" class="orbit orbit-wrapper" role="region" data-orbit data-resize='home-slider'>
        <ul class="orbit-container">
            <button class="orbit-previous orbit-wrapper__button orbit-wrapper__button--prev">
                <img src="<?= get_template_directory_uri(); ?>/dist/images/left-arrow.png" alt="<?php echo esc_html('left Tickets');?>">
            </button>
            <button class="orbit-next orbit-wrapper__button orbit-wrapper__button--next">
                <img src="<?= get_template_directory_uri(); ?>/dist/images/right-arrow.png" alt="<?php echo esc_html('right arrow');?>">
            </button>
        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
            //get your acf meta fields
            
         ?>
            <?php setup_postdata($post); $typeOfpost = get_post_type();?>
            <?php if ($typeOfpost == 'production'):?>
                <li class="orbit-slide orbit-wrapper__slide pfc pfc--purple">
               
                    <?php get_template_part('partials/programming-featured-content'); //pfc for short (css naming) ?>

                </li>
            <?php elseif($typeOfpost == 'reviews'):?>
                    <li class="orbit-slide orbit-wrapper__slide pfc pfc--purple">
               
                        <?php get_template_part('partials/home/reviews'); //pfc for short (css naming) ?>

                    </li>
            <?php else:?>
                <li class="orbit-slide orbit-wrapper__slide pfc pfc--purple">
               
                        <?php get_template_part('partials/home/even-slide'); //pfc for short (css naming) ?>

                    </li>
            <?php endif;?>
        <?php endforeach; ?>
        </ul>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>