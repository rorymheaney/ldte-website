<?php
	$venueName = esc_html( get_post_meta( get_the_ID(), 'venue', true ) );
    $venueAddress = esc_html( get_post_meta(get_the_ID(), 'address', true) );
    $startDate = get_post_meta( get_the_ID(), 'start_date', true );
    $endDate = get_post_meta( get_the_ID(), 'end_date', true );
    $ticketsLink = esc_url( get_post_meta(get_the_ID(), 'external_link', true) );
    $ticketsOption = get_post_meta(get_the_ID(), 'buy_tickets', true);
    $productionBy = get_post_meta(get_the_ID(), 'written_by', true);
    $timeOfProduction = get_post_meta( get_the_ID(), 'time_of_production', true );
    //format dates
    $startFormatted = date("j F",strtotime($startDate));
    $endFormatted = date("j F",strtotime($endDate));
    $endFormatYearOnly = date("Y",strtotime($endDate));

    $homeSlideImage = get_post_meta( get_the_ID(), 'home_page_featured', true );
	$homeSlideImage = json_decode($homeSlideImage);
	$homeSlideImage = $homeSlideImage->cropped_image;
?>

<div class="pfc__image-wrapper">
	<div class="pfc__image" style="background-image:url(<?php if($homeSlideImage):?><?php echo wp_get_attachment_url( $homeSlideImage,'full' ) ?><?php else:?><?php the_post_thumbnail_url('full'); ?><?php endif;?>);"></div>
    
</div>

<div class="pfc__content pfc__content--event-preview">
	<span class="pfc__misc">
	        <?php echo esc_html('Event');?>
	    </span>

	    <h2 class="pfc__title pfc__title--plus">
        <?php the_title(); ?> 
    </h2>

    <p class="pfc__date">
	        <?php echo $startFormatted;?> <?php if($endDate):?>- <?php echo $endFormatted;?><?php endif;?> <?php if($timeOfProduction):?>- <?php echo date("g:i a", strtotime($timeOfProduction));?><?php endif;?>
	    </p>

    <?php the_excerpt();?>

        <?php if(get_field('external_link')):?>

            <a class="button button--tickets" href="<?php the_field('external_link');?>">
                <?php echo esc_html('Get Tickets');?>
            </a>
        <?php endif;?>
        
    <a class="button button--tickets" href="<?php the_permalink();?>">
	        <?php echo esc_html('Learn More');?>
	    </a>

</div>