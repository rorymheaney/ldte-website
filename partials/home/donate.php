<?php 
	$titleC = esc_html( get_post_meta( get_the_id(), 'title_donate', true ) );
	$contentC = get_post_meta( get_the_id(), 'description_donate', true);
	$linkTextC = esc_html( get_post_meta( get_the_id(), 'link_text_donate', true ) );
	$pageLinkC = get_post_meta( get_the_id(), 'link_page_donate', true );
	$imageC = (int) get_post_meta( get_the_ID(), 'image_donate', true );
	$imageTwoC = (int) get_post_meta( get_the_ID(), 'image_donate_two', true );
?>
<div class="front-block front-block--donate">
	<div class="front-block__mobile-img show-for-small-only">
		<?php echo wp_get_attachment_image( $imageC,'medium_large' ) ?>
	</div>
	<div class="front-block__images">
		<?php echo wp_get_attachment_image( $imageC,'full' ) ?>

		<?php echo wp_get_attachment_image( $imageTwoC,'full', "", ["class" => "front-block__img-position front-block__img-position--four"] ) ?>
		<img data-100-bottom="top: -200px;" data--800-bottom="top: 75px;" class="front-block__img-position front-block__img-position--one" src="<?= get_template_directory_uri(); ?>/dist/images/circleA.png">
		<img data-200-bottom="right: -165px;" data--700-bottom="right: 0px;" class="front-block__img-position front-block__img-position--two" src="<?= get_template_directory_uri(); ?>/dist/images/imageC.png">
		<img data-200-bottom="bottom: -10px;" data--700-bottom="bottom: 150px;" class="front-block__img-position front-block__img-position--three" src="<?= get_template_directory_uri(); ?>/dist/images/image2C.png">
	</div>

	<div class="front-block__content end">
		<h2 class="front-block__title">
			<?php echo $titleC;?>
		</h2>
		<p class="front-block__copy">
			<span>—</span><?php echo $contentC;?>
		</p>

		<a class="front-block__page-link" href="<?php echo get_page_link($pageLinkC);?>">
			<?php echo $linkTextC;?>
		</a>
	</div>

	
</div>