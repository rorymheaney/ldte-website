<?php
	// $theirName = esc_html( get_post_meta( get_the_ID(), 'name', true ) );
	$headshot = get_post_meta( get_the_ID(), 'production_single_page', true );
	$headshot = json_decode($headshot);
	$headshot = $headshot->cropped_image;
	$headshotBig = get_post_meta( get_the_ID(), 'main_featured_crop', true );
	$headshotBig = json_decode($headshotBig);
	$headshotBig = $headshotBig->cropped_image;

	$actorTitle = esc_html(get_post_meta( get_the_ID(), 'title', true ));
	$actorBio = esc_html(get_post_meta( get_the_ID(), 'bio', true ));
	$actorIMDB = esc_url(get_post_meta( get_the_ID(), 'imdb_link', true ));
	//if statements
	$isVisionTemplate = is_page_template('template-vision.php');
?>

<li class="actors-list__actor clearfix">
  	
  	<?php if ($isVisionTemplate):?>
  		<div class="actors-list__image-wrapper">
  			<?php echo wp_get_attachment_image( $headshotBig,'full' ) ?>
  		</div>
  	<?php else:?>
  		<?php echo wp_get_attachment_image( $headshot,'full' ) ?>
  	<?php endif;?>

  	<div class="actors-list__info">

  		<?php if ($isVisionTemplate):?>
  			<span class="actors-list__title">
  				<?php echo $actorTitle;?>
  			</span>
  		<?php endif;?>

	    <span class="actors-list__name <?php if ($isVisionTemplate):?>actors-list__name--big<?php else:?>actors-list__name--small<?php endif;?>">
			<?php the_title();?>
		</span>

		<?php if ($isVisionTemplate):?>
  			<p class="actors-list__bio">
  				<?php echo $actorBio;?>
  			</p>
  		<?php endif;?>

  		<?php if ($isVisionTemplate):?>
  			<a class="actors-list__read-more" href="<?php echo $actorIMDB;?>" target="_blank">
  				<?php echo esc_html('Read More');?>
  			</a>
  		<?php endif;?>
	</div>
</li>