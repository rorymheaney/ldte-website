<?php
	$venueName = esc_html( get_post_meta( get_the_ID(), 'venue', true ) );
    $venueAddress = esc_html( get_post_meta(get_the_ID(), 'address', true) );
    $startDate = get_post_meta( get_the_ID(), 'start_date', true );
    $endDate = get_post_meta( get_the_ID(), 'end_date', true );
    $ticketsLink = esc_url( get_post_meta(get_the_ID(), 'external_link', true) );
    $ticketsOption = get_post_meta(get_the_ID(), 'buy_tickets', true);
    $productionBy = get_post_meta(get_the_ID(), 'written_by', true);
    //format dates
    $startFormatted = date("j F",strtotime($startDate));
    $endFormatted = date("j F",strtotime($endDate));
    $endFormatYearOnly = date("Y",strtotime($endDate));
    $timeOfProduction = get_post_meta( get_the_ID(), 'time_of_production', true );
    //if statements
    $templateANDAcfORFrontPage = is_page_template('template-custom.php') && $ticketsOption === 'buy-now' || is_front_page();

    $homeSlideImage = get_post_meta( get_the_ID(), 'home_page_featured', true );
	$homeSlideImage = json_decode($homeSlideImage);
	$homeSlideImage = $homeSlideImage->cropped_image;
?>

	
<div class="pfc__image-wrapper">
	<?php if ( $templateANDAcfORFrontPage):?>
		<div class="pfc__image" style="background-image:url(<?php if($homeSlideImage):?><?php echo wp_get_attachment_url( $homeSlideImage,'full' ) ?><?php else:?><?php the_post_thumbnail_url('full'); ?><?php endif;?>);"></div>
	<?php else:?>
		<div class="pfc__image" style="background-image:url(<?php if($homeSlideImage):?><?php echo wp_get_attachment_url( $homeSlideImage,'medium_large' ) ?><?php else:?><?php the_post_thumbnail_url('medium_large'); ?><?php endif;?>);">
			<a class="hide" href="<?php the_permalink();?>"></a>
		</div>
	<?php endif;?>
    
</div>
<div class="pfc__content">

	<?php if(is_front_page() && !is_singular('post') ):?>
	    <span class="pfc__misc">
	        <?php echo esc_html('Productions');?>
	    </span>
	<?php endif;?>

    <h2 class="pfc__title <?php if ( $templateANDAcfORFrontPage):?>pfc__title--plus<?php endif;?>">
        <?php the_title(); ?> 
    </h2>

    <?php if ( is_page_template('template-custom.php') && $ticketsOption === 'buy-now'):?>
		<div class="pfc__writer">
			<span>
				<?php echo esc_html('Stage Production  -  ');?> <?php echo $productionBy;?>
			</span>
		</div>
	<?php elseif(is_page_template('template-custom.php')):?>
		<div class="pfc__writer">
			<span>
				<?php echo esc_html('By');?> <?php echo $productionBy;?> - <?php echo $endFormatYearOnly;?>
			</span>
		</div>
	<?php endif;?>

	<?php if ( $templateANDAcfORFrontPage):?>
		<p class="pfc__date">
	        <?php echo $startFormatted;?> <?php if($endDate):?>- <?php echo $endFormatted;?><?php endif;?> <?php if($timeOfProduction):?>- <?php echo date("g:i a", strtotime($timeOfProduction));?><?php endif;?>
	    </p>
	
	    <div class="pfc__meta">
	        <span>
	            <?php echo $venueName; ?> <br/>
	            <?php echo $venueAddress;?>
	        </span>
	    </div>

	<?php endif;?>
    
    

    <?php the_content();?>

    <?php if ( $templateANDAcfORFrontPage):?>
	
		<?php if(get_field('external_link')):?>

			<a class="button button--tickets" href="<?php the_field('external_link');?>">
				<?php echo esc_html('Get Tickets');?>
			</a>
		<?php endif;?>

	    <a class="button button--tickets" href="<?php the_permalink();?>">
	        <?php echo esc_html('Learn More');?>
	    </a>

    <?php endif;?>
</div>